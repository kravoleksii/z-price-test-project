import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CheckboxService} from '../core/services/checkbox.service';
import {ApiService} from '../core/services/api.service';
import {LocalStorageService} from '../core/services/local-storage.service';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    CheckboxService,
    ApiService,
    LocalStorageService
  ]
})
export class PagesModule { }

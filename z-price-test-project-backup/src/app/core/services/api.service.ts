import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {LocalStorageService} from "./local-storage.service";
import {Subject} from "rxjs";
import {CheckboxService} from './checkbox.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constUrls: string = 'https://randomuser.me/api/?page=25&results=100&seed=abc&inc=id,name,picture,';
  apiUrl = '';
  subject = new Subject();

  constructor(
    private http: HttpClient,
    private localStorage: LocalStorageService,
    private checkbox: CheckboxService) {
  }

  getUsersData(): any {
    this.generateUrl();
    return this.http.get(this.apiUrl);
  }

  generateUrl() {
    let filters = this.checkbox.getFilters();
    let gender = filters.gender ? 'gender,' : '';
    let location = filters.location ? 'location,' : '';
    let email = filters.email ? 'email,' : '';
    let phone = filters.phone ? 'phone,' : '';
    this.apiUrl = `${this.constUrls}${gender}${location}${email}${phone}`;
  }
}

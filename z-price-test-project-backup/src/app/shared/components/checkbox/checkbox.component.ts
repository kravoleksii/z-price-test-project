import {Component, OnInit} from '@angular/core';
import {LocalStorageService} from '../../../core/services/local-storage.service';
import {ApiService} from '../../../core/services/api.service';
import {CheckboxService} from '../../../core/services/checkbox.service';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss']
})
export class CheckboxComponent implements OnInit {
  checkboxFilters;

  constructor(
    private localStorage: LocalStorageService,
    private api: ApiService,
    private checkbox: CheckboxService
  ) {
  }

  ngOnInit(): void {
    this.initFilters();
  }

  initFilters() {
    this.checkboxFilters = this.checkbox.getFilters();
  }

  submit() {
    this.checkbox.setFilters(this.checkboxFilters);
    this.api.subject.next();
  }
}

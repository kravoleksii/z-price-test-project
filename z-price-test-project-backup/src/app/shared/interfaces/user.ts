export interface User {
  email: string;
  gender: string;
  location: {
    city: string;
    coordinates: {
      latitude: string;
      longitude: string;
    };
    country: string;
    postcode: number;
    state: string;
    street: {
      number: number;
      name: string;
    };
    timezone: {
      offset: string;
      description: string;
    };
    name: string;
    picture: {
      large: string;
      medium: string;
      thumbnail: string;
    };
  }
}

import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../../core/services/api.service';
import {User} from '../../../shared/interfaces/user';
import DataSource from 'devextreme/data/data_source';
import ArrayStore from 'devextreme/data/array_store';
import {LocalStorageService} from '../../../core/services/local-storage.service';
import {CheckboxService} from '../../../core/services/checkbox.service';


@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {
  users: User[];
  tasksDataSourceStorage: any = [];
  filters: object;

  constructor(
    private api: ApiService,
    private localStorage: LocalStorageService,
    private checkbox: CheckboxService
  ) {
  }

  ngOnInit() {
    this.checkbox.initFilters();
    this.api.subject
      .subscribe(() => {
        this.getFilters();
        this.getUsersData();
      });
    this.getFilters();
    this.getUsersData();
  }


  getUsersData() {
    this.api.getUsersData()
      .subscribe((result) => {
        this.users = result['results']
          // Create a full name using name.first and name.last;
          .map((user) => {
            user.name = `${user.name.first} ${user.name.last}`;
            // Create and round ID for users w/o ID;
            if (!user.id.value) {
              user.id.value = Math.floor(Math.random() * 1000000000,);
              user.id.name = 'RAN';
            }
            return user;
          });
      });
  }

  getFilters() {
    this.filters = this.checkbox.getFilters();
  }


  getDetails(key) {
    let item = this.tasksDataSourceStorage.find((i) => i.key === key);
    if (!item) {
      item = {
        key: key,
        dataSourceInstance: new DataSource({
          store: new ArrayStore({
            data: this.users,
            key: 'id'
          }),
          filter: ['id', '=', key]
        })
      };
      this.tasksDataSourceStorage.push(item);
    }
    return item.dataSourceInstance;
  }

}

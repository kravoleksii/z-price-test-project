import {Injectable} from '@angular/core';
import {LocalStorageService} from './local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class CheckboxService {

  filters;

  constructor(
    private localStorage: LocalStorageService
  ) {
  }

  initFilters() {
    let localFilters = this.localStorage.getFromLocalStorage('filters');
    if (localFilters) {
      this.filters = JSON.parse(localFilters);
    } else {
      this.filters = {
        gender: true,
        location: true,
        email: true,
        phone: true
      };
      this.localStorage.setToLocalStorage('filters', JSON.stringify(this.filters));
    }
  }

  getFilters() {
    return this.filters;
  }

  setFilters(filters) {
    this.filters = filters;
    this.localStorage.setToLocalStorage('filters', JSON.stringify(this.filters));
  }
}


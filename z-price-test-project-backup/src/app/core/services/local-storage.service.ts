import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() {
  }

  setToLocalStorage(key, value) {
    localStorage.setItem(key, value);
  }

  getFromLocalStorage(key) {
    return localStorage.getItem(key);
  }
}


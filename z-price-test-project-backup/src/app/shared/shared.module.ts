import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CheckboxComponent} from './components/checkbox/checkbox.component';
import {FormsModule} from "@angular/forms";


@NgModule({
  declarations: [CheckboxComponent],
  exports: [
    CheckboxComponent
  ],
    imports: [
        CommonModule,
        FormsModule
    ]
})
export class SharedModule {
}

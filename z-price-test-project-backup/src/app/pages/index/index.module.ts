import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {IndexRoutingModule} from './index-routing.module';
import {IndexComponent} from './index/index.component';
import {DevExtremeModule, DxBulletModule, DxButtonModule, DxDataGridModule, DxTemplateModule} from 'devextreme-angular';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  declarations: [IndexComponent],
  imports: [
    CommonModule,
    IndexRoutingModule,
    DxDataGridModule,
    DxButtonModule,
    DxTemplateModule,
    DxBulletModule,
    DevExtremeModule,
    SharedModule
  ]
})
export class IndexModule {
}
